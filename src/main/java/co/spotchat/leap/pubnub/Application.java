package co.spotchat.leap.pubnub;

import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.PubNubException;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.enums.PNLogVerbosity;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.access_manager.PNAccessManagerGrantResult;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class Application {
    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws PubNubException {
        LOG.debug("pubnub");
        grant();
    }

    private static void grant() throws PubNubException {

        PNConfiguration pnConfiguration = new PNConfiguration()
                .setSubscribeKey("sub-c-128d1eee-5b64-11e7-b83d-02ee2ddab7fe")
                .setPublishKey("pub-c-fd015b87-3d6a-4f30-bf09-2cf4837e6438")
                .setSecretKey("sec-c-MmRhOWVmOWEtODViZi00ZGRjLThiOTQtZDdiODlhYjQyNGZj")
                .setSecure(true)
                .setLogVerbosity(PNLogVerbosity.BODY);

        List<String> channelList = Arrays.asList("L123".split(","));

        PubNub pn = new PubNub(pnConfiguration);

        // For grants, read or write permissions need to be set to true /
        PNAccessManagerGrantResult x = pn.grant()
                .channels(channelList)
                .authKeys(Arrays.asList("12345"))
                .read(true)
                .write(true)
                .ttl(10080)
                .sync();
//        pn.grant()
//                .channels(channelList)
//                .authKeys(Arrays.asList("12345"))
//                .read(true)
//                .write(true)
//                .ttl(10080)
//                .async(new PNCallback<PNAccessManagerGrantResult>() {
//                    @Override
//                    public void onResponse(PNAccessManagerGrantResult x, PNStatus pns) {
//                        LOG.debug("rws result {}", x);
//                        LOG.debug("rws status {}", pns);
//                    }
//                });
    }
}
